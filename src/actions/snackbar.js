export function showSnackbar(message) {
  return {
    type: 'SHOW_SNACKBAR',
    payload: message
  }
}

export function hideSnackbar() {
  return {type: 'HIDE_SNACKBAR'}
}
