export function setBackPage(path) {
  return {
    type: 'SET_BACK_PAGE',
    payload: path,
  }
}

export function unsetBackPage() {
  return {
    type: 'SET_BACK_PAGE',
    payload: null,
  }
}
