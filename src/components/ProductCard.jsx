import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import {
  Card as MuiCard,
  CardActionArea,
  CardContent as MuiCardContent,
  CardMedia as MuiCardMedia,
  Typography as MuiTypography
} from '@material-ui/core'

import {forMobile} from '../utils'
import LongClickWrapper from './LongClickWrapper'

const Card = styled(MuiCard)`
  max-width: 240px;
  margin-top: 1em;
  
  ${forMobile} {
    max-width: 150px;
  }
`

const CardContent = styled(MuiCardContent)`
  display: flex;
  justify-content: space-between;
`

const CardMedia = styled(MuiCardMedia)`
  height: 140px;
  object-fit: cover;
  
  ${forMobile} {
    height: 100px;
  }
`

const Typography = styled(MuiTypography)`
  text-overflow: ellipsis;
  
  ${forMobile} {
    font-size: 1rem !important;
  }
`

class ProductCard extends React.Component {
  constructor(props) {
    super(props)

    this.handleLongClick = this.handleLongClick.bind(this)
  }

  handleLongClick(e) {
    const {product, onLongClick} = this.props
    onLongClick(e, product)
  }

  render() {
    const {product: {image}, label} = this.props
    return (
      <LongClickWrapper component={Card} onLongClick={this.handleLongClick}>
        <CardActionArea>
          <CardMedia component="span" image={image}/>
          <CardContent>
            <Typography variant="h5" component="span">{label}</Typography>
          </CardContent>
        </CardActionArea>
      </LongClickWrapper>
    )
  }
}

ProductCard.propTypes = {
  label: PropTypes.string.isRequired,
  onLongClick: PropTypes.func.isRequired,
  product: PropTypes.shape({
    image: PropTypes.string.isRequired,
  })
}

export default ProductCard
