import React from 'react'
import Quagga from 'quagga'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import {Close} from '@material-ui/icons'
import {IconButton, Paper as MuiPaper} from '@material-ui/core'

import {forMobile} from '../utils'

const className = 'barCodeReader'

const config = {
  inputStream: {
    type: 'LiveStream',
    target: '.' + className,
  },
  decoder: {
    readers: ['code_128_reader', 'ean_reader', 'ean_8_reader'],
  }
}

const Paper = styled(MuiPaper)`
  left: 50%;
  z-index: 1;
  width: 40em;
  position: absolute;
  transform: translate(-50%, -40%);
  padding: .5em .5em .3em .5em;
  
  ${forMobile} {
    width: 20em;
    transform: translate(-50%, -30%);
  }

  video {
    width: 100%;
  }
  
  canvas {
    left: 0;
    position: absolute;
  }
`

class BarCodeReader extends React.Component {
  componentDidMount() {
    Quagga.onProcessed(drawLines)
    Quagga.onDetected(this.props.onDetected)
    Quagga.init(config, initializeReader)
  }

  componentWillUnmount() {
    Quagga.offDetected(this.props.onDetected)
    Quagga.stop()
  }

  render() {
    return (
      <Paper className={className}>
        <IconButton position="right" onClick={this.props.onClose}>
          <Close/>
        </IconButton>
      </Paper>
    )
  }
}

BarCodeReader.propTypes = {
  onClose: PropTypes.func.isRequired,
  onDetected: PropTypes.func.isRequired,
}

const BarCodeReaderRenderer = props => {
  const {open, ..._props} = props
  return open ? <BarCodeReader {..._props}/> : null
}

BarCodeReaderRenderer.propTypes = Object.assign({open: PropTypes.bool.isRequired}, BarCodeReader.propTypes)

export default BarCodeReaderRenderer

function initializeReader(err) {
  if (err) {
    return
  }
  Quagga.start()
}

function drawLines(result) {
  const drawingCtx = Quagga.canvas.ctx.overlay
  const drawingCanvas = Quagga.canvas.dom.overlay

  if (!result) {
    return
  }

  if (result.boxes) {
    const width = parseInt(drawingCanvas.getAttribute('width'))
    const height = parseInt(drawingCanvas.getAttribute('height'))
    drawingCtx.clearRect(0, 0, width, height)

    result.boxes.filter(box => box !== result.box).forEach(box => {
      Quagga.ImageDebug.drawPath(box, {x: 0, y: 1}, drawingCtx, {color: 'green', lineWidth: 2})
    })
  }

  if (result.box) {
    Quagga.ImageDebug.drawPath(result.box, {x: 0, y: 1}, drawingCtx, {color: '#00F', lineWidth: 2})
  }

  if (result.codeResult && result.codeResult.code) {
    Quagga.ImageDebug.drawPath(result.line, {x: 'x', y: 'y'}, drawingCtx, {color: 'red', lineWidth: 3})
  }
}
