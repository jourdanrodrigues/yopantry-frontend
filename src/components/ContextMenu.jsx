import React from 'react'
import {Menu as MuiMenu} from '@material-ui/core'
import styled from 'styled-components'

const Menu = styled(MuiMenu)`
  transform: translateX(2em);
`

const ContextMenu = props => {
  const {anchorEl, children, ..._props} = props
  return (
    <Menu anchorEl={anchorEl} open={!!anchorEl} {..._props}>
      {children}
    </Menu>
  )
}

export default ContextMenu
