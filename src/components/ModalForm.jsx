import React from 'react'
import PropTypes from 'prop-types'
import {Modal} from '@material-ui/core'
import styled from 'styled-components'

import {forDesktop} from '../utils'

const Form = styled.form`
  top: 30%;
  left: 50%;
  width: 15em;
  padding: 2em;
  display: flex;
  position: absolute;
  border-radius: 3px;
  flex-direction: column;
  background-color: white;
  transform: translateX(-50%);
  box-shadow: 0 .5em 1em rgba(0, 0, 0, .5);
  
  ${forDesktop} {
    width: 30em;
  }
`

const ModalForm = props => {
  const {onSubmit, children, ..._props} = props
  return (
    <Modal {..._props}>
      <Form onSubmit={onSubmit}>{children}</Form>
    </Modal>
  )
}

ModalForm.propTypes = {
  onSubmit: PropTypes.func,
  children: PropTypes.oneOfType([
    PropTypes.node,
    PropTypes.arrayOf(PropTypes.node),
  ])
}

export default ModalForm
