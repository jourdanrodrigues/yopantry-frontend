import React from 'react'
import PropTypes from 'prop-types'
import {PhotoCameraOutlined} from '@material-ui/icons'
import {FormControl, IconButton, Input, InputAdornment, InputLabel} from '@material-ui/core'

import BarCodeReader from './BarCodeReader'

const SideButtonReader = props => (
  <InputAdornment position="end">
    <IconButton aria-label="Scan bar code" onClick={props.onClick}>
      <PhotoCameraOutlined/>
    </IconButton>
  </InputAdornment>
)

SideButtonReader.propTypes = {
  onClick: PropTypes.func.isRequired,
}

class BarCodeField extends React.Component {
  state = {
    readerOpen: false,
    codes: {} // {[code]: count}
  }

  constructor(props) {
    super(props)

    this.openReader = this.openReader.bind(this)
    this.updateCode = this.updateCode.bind(this)
    this.closeReader = this.closeReader.bind(this)
  }

  openReader() {
    this.setState({readerOpen: true})
  }

  closeReader() {
    this.setState({readerOpen: false, codes: {}})
  }

  updateCode(result) {
    const {codes} = this.state
    const codeRead = result.codeResult.code

    if (!codes[codeRead]) {
      codes[codeRead] = 1
      return
    }

    codes[codeRead] += 1

    if (codes[codeRead] === 5) {
      const fakeElement = {target: {value: codeRead}}
      this.props.onChange(fakeElement)
      this.closeReader()
    }
  }

  render() {
    const {className, ...props} = this.props
    return (
      <FormControl className={className}>
        <InputLabel htmlFor="adornment-password">Bar code</InputLabel>
        <Input {...props} endAdornment={<SideButtonReader onClick={this.openReader}/>}/>
        <BarCodeReader open={this.state.readerOpen} onDetected={this.updateCode} onClose={this.closeReader}/>
      </FormControl>
    )
  }
}

BarCodeField.propTypes = {
  onChange: PropTypes.func.isRequired,
  className: PropTypes.string,
}

export default BarCodeField
