import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import {Button as MuiButton} from '@material-ui/core'

const buttonSpacing = '1em'
const Button = styled(MuiButton)`
  right: ${buttonSpacing};
  bottom: ${buttonSpacing};
  position: fixed !important;
`

const FloatingActionButton = props => {
  const {children, ..._props} = props
  return (
    <Button variant="fab" {..._props}>
      {children}
    </Button>
  )
}

FloatingActionButton.defaultProps = {
  color: 'primary',
}

FloatingActionButton.propTypes = {
  children: PropTypes.node.isRequired,
}

export default FloatingActionButton
