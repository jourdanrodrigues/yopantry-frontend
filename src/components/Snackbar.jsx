import React from 'react'
import PropTypes from 'prop-types'
import {connect} from 'react-redux'
import {bindActionCreators} from 'redux'
import {Snackbar as MuiSnackbar} from '@material-ui/core'

import {hideSnackbar} from '../actions'

const Snackbar = props => {
  const {hideSnackbar, snackbar} = props
  return <MuiSnackbar onClose={hideSnackbar} autoHideDuration={6000} {...snackbar}/>
}

Snackbar.propTypes = {
  hideSnackbar: PropTypes.func.isRequired,
  snackbar: PropTypes.shape({
    message: PropTypes.string,
    open: PropTypes.bool.isRequired,
  }).isRequired,
}

export default connect(mapStateToProps, mapDispatchToProps)(Snackbar)

function mapStateToProps(state) {
  return {snackbar: state.snackbar}
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({hideSnackbar}, dispatch)
}
