import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import {List as MuiList} from '@material-ui/core'

const List = styled(MuiList)`
  display: flex;
  flex-wrap: wrap;
  justify-content: space-evenly;
`

const ProductList = ({children}) => {
  return <List>{children}</List>
}

ProductList.propTypes = {
  children: PropTypes.arrayOf(
    PropTypes.node,
  ),
}

export default ProductList
