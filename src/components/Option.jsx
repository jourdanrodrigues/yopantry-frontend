import React from 'react'
import PropTypes from 'prop-types'
import {MenuItem} from '@material-ui/core'

class Option extends React.Component {
  constructor(props) {
    super(props)

    this.selectOption = this.selectOption.bind(this)
  }

  selectOption(event) {
    const {onSelect, option} = this.props
    onSelect(option, event)
  }

  render() {
    const {children, isFocused, isSelected, onFocus} = this.props
    const style = {fontWeight: isSelected ? 500 : 400}
    return (
      <MenuItem onFocus={onFocus} selected={isFocused} onClick={this.selectOption} component="div" style={style}>
        {children}
      </MenuItem>
    )
  }
}

Option.propTypes = {
  option: PropTypes.any.isRequired,
  onFocus: PropTypes.func.isRequired,
  onSelect: PropTypes.func.isRequired,
  isFocused: PropTypes.bool.isRequired,
  isSelected: PropTypes.bool.isRequired,
  children: PropTypes.oneOfType([
    PropTypes.node,
    PropTypes.arrayOf(PropTypes.node),
  ]),
}

export default Option
