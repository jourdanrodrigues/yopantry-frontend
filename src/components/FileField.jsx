import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import {CloudUpload as MuiCloudUploadIcon} from '@material-ui/icons'
import {Button} from '@material-ui/core'

const CloudUploadIcon = styled(MuiCloudUploadIcon)`
  margin-right: .5em;
`

const HiddenLabel = styled.label`
  display: none;
`

class FileField extends React.Component {
  state = {
    fileInputEl: null,
  }

  constructor(props) {
    super(props)

    this.setFileInputEl = this.setFileInputEl.bind(this)
    this.clickFileInputEl = this.clickFileInputEl.bind(this)
  }

  setFileInputEl(e) {
    this.setState({fileInputEl: e})
  }

  clickFileInputEl() {
    this.state.fileInputEl.click()
  }

  render() {
    const {className, color, fullWidth, label, onChange, value} = this.props
    const buttonText = value ? value.replace(/.+\\/g, '') : label
    return (
      <div className={className}>
        <Button component="span"
                color={color}
                variant="contained"
                fullWidth={fullWidth}
                onClick={this.clickFileInputEl}>
          <CloudUploadIcon/>
          {buttonText}
        </Button>
        <HiddenLabel>
          <input ref={this.setFileInputEl} accept="image/*" type="file" onChange={onChange}/>
        </HiddenLabel>
      </div>
    )
  }
}

FileField.propTypes = {
  multiple: PropTypes.bool,
  fullWidth: PropTypes.bool,
  className: PropTypes.string,
  label: PropTypes.string.isRequired,
  value: PropTypes.string.isRequired,
  onChange: PropTypes.func.isRequired,
}

export default FileField
