import React from 'react'
import {TextField as MuiTextField} from '@material-ui/core'

const TextField = props => {
  const {InputLabelProps, ..._props} = props
  return <MuiTextField InputLabelProps={{required: false, ...InputLabelProps}} {..._props}/>
}

export default TextField
