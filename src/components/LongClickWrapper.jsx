import React from 'react'
import PropTypes from 'prop-types'

class LongClickWrapper extends React.Component {
  state = {
    timeoutId: null,
  }

  constructor(props) {
    super(props)

    this.onTouchEnd = this.onTouchEnd.bind(this)
    this.cancelAction = this.cancelAction.bind(this)
    this.triggerAction = this.triggerAction.bind(this)
  }

  triggerAction(e) {
    e.persist()

    const {onLongClick, holdDuration} = this.props
    const callAction = () => {
      onLongClick(e)
      this.setState({timeout: null})
    }

    const timeoutId = setTimeout(callAction, holdDuration)
    this.setState({timeoutId})
  }

  cancelAction() {
    const {timeoutId} = this.state
    if (timeoutId) {
      clearTimeout(timeoutId)
      this.setState({timeoutId: null})
    }
  }

  onTouchEnd(e) {
    e.preventDefault()
    this.cancelAction()
  }

  render() {
    const {component: Component, children, ...props} = this.props
    const cleanedProps = cleanPropsToRender(props)
    return (
      <Component onMouseDown={this.triggerAction}
                 onMouseUp={this.cancelAction}
                 onTouchStart={this.triggerAction}
                 onTouchMove={this.cancelAction}
                 onTouchEnd={this.onTouchEnd}
                 {...cleanedProps}>
        {children}
      </Component>
    )
  }
}

LongClickWrapper.defaultProps = {
  holdDuration: 500,
}

LongClickWrapper.propTypes = {
  component: PropTypes.object.isRequired,
  onLongClick: PropTypes.func.isRequired,
  holdDuration: PropTypes.number,
}

export default LongClickWrapper

function cleanPropsToRender(props) {
  // noinspection JSUnusedLocalSymbols
  const {onMouseUp, onMouseDown, onTouchStart, onTouchEnd, holdDuration, onLongClick, ..._props} = props
  return _props
}
