import * as Sentry from '@sentry/browser'

const PRODUCTION = process.env.NODE_ENV === 'production'
const {
  REACT_APP_SENTRY_DSN: dsn,
  REACT_APP_SENTRY_ENV: environment,
} = process.env

if (PRODUCTION && !(dsn && environment)) {
  console.warn('Sentry should be defined in production environments')
} else {
  Sentry.init({environment, dsn})
}
