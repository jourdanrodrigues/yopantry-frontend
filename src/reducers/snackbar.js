const closedState = {open: false, message: null}

export default function(state = closedState, {type, payload}) {
  switch (type) {
    case 'SHOW_SNACKBAR':
      return {
        open: true,
        message: payload,
      }
    case 'HIDE_SNACKBAR':
      return closedState
    default:
      return state
  }
}
