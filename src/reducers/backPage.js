export default function(state = null, {type, payload}) {
  switch (type) {
    case 'SET_BACK_PAGE':
      return payload
    default:
      return state
  }
}
