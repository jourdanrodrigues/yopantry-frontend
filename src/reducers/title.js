const defaultTitle = 'YoPantry'

export default function(state = defaultTitle, {type, payload: title}) {
  switch (type) {
    case 'SET_TITLE':
      return title || defaultTitle
    default:
      return state
  }
}
