import {combineReducers} from 'redux'

import TitleReducer from './title'
import BackPageReducer from './backPage'
import SnackbarReducer from './snackbar'

export default combineReducers({
  title: TitleReducer,
  backPage: BackPageReducer,
  snackbar: SnackbarReducer,
})
