import React from 'react'
import styled from 'styled-components'
import {HashRouter, Redirect, Route, Switch} from 'react-router-dom'

import {AppBar} from './containers'
import {Snackbar} from './components'
import {AuthPage, PantryProductView, PantryView, ProductView} from './views'

const Root = styled.div`
  width: 100%;
  height: 100%;
`

const App = () => (
  <HashRouter>
    <Root>
      <Switch>
        <Route exact path="/" render={LoginRedirectRenderer}/>
        <Route exact path="/login/" component={AuthPage}/>
        <Route path="/app/" component={AppBar}/>
      </Switch>
      <Switch>
        <Route exact path="/app/pantries/" component={PantryView}/>
        <Route exact path="/app/products/" component={ProductView}/>
        <Route exact path="/app/pantries/:pantryId/products/" component={PantryProductView}/>
      </Switch>
      <Snackbar/>
    </Root>
  </HashRouter>
)

export default App

function LoginRedirectRenderer() {
  return <Redirect to="/login/"/>
}
