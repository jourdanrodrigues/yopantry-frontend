import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import {Button as MuiButton, Input} from '@material-ui/core'

import client from '../api'
import {delayCall} from '../utils'
import {ModalForm as RawModalForm, Select, TextField as CustomTextField} from '../components'

const TextField = styled(CustomTextField)``

const ModalForm = styled(RawModalForm)`
  ${TextField}:not(:first-child) {
    margin-top: .5em;
  }
`

const Button = styled(MuiButton)`
  margin-top: 2em !important;
`

class PantryProductAdditionModal extends React.Component {
  state = {
    option: '', // Object: {label, value}
    quantity: '',
    products: [],
  }

  constructor(props) {
    super(props)

    this.addProducts = this.addProducts.bind(this)
    this.updateOption = this.updateOption.bind(this)
    this.updateQuantity = this.updateQuantity.bind(this)
    this.fetchProductsByName = delayCall(this.fetchProductsByName.bind(this), 500)
  }

  updateOption(option) {
    this.setState({option})
  }

  updateQuantity({target: {value: quantity}}) {
    this.setState({quantity})
  }

  addProducts(e) {
    e.preventDefault()

    const {option: product, quantity} = this.state
    const {pantryId, onClose, onAddition} = this.props

    const data = [
      {product: product.id, quantity},
    ]

    client.addProductsToPantry(pantryId, data).then(onClose).then(onAddition)
  }

  fetchProductsByName(name) {
    client
      .getProductsByName(name)
      .then(products => {
        this.setState({products: products.map(promoteProductToOption)})
      })
  }

  render() {
    const {open, onClose} = this.props
    const {option, products, quantity} = this.state
    const inputProps = {
      value: option,
      options: products,
      onInputChange: this.fetchProductsByName,
    }
    return (
      <ModalForm open={open} onClose={onClose} onSubmit={this.addProducts}>
        <Input required
               inputComponent={Select}
               onChange={this.updateOption}
               placeholder="Select products"
               inputProps={inputProps}/>
        <TextField required
                   type="number"
                   label="Quantity"
                   value={quantity}
                   onChange={this.updateQuantity}
                   inputProps={{step: 0.5, min: 0}}/>
        <Button variant="contained" color="primary" type="submit">Add</Button>
      </ModalForm>
    )
  }
}

PantryProductAdditionModal.propTypes = {
  open: PropTypes.bool.isRequired,
  onClose: PropTypes.func.isRequired,
  pantryId: PropTypes.string.isRequired,
  onAddition: PropTypes.func.isRequired,
}

export default PantryProductAdditionModal

function promoteProductToOption(product) {
  const {id: value, name: label} = product
  return {value, label, ...product}
}
