import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import {Button as MuiButton, MenuItem} from '@material-ui/core'

import client from '../api'
import {ModalForm as RawModalForm, TextField as CustomTextField} from '../components'

const TextField = styled(CustomTextField)``

const ModalForm = styled(RawModalForm)`
  ${TextField}:not(:first-child) {
    margin-top: .5em;
  }
`

const Button = styled(MuiButton)`
  margin-top: 2em !important;
`

class PantryProductWithdrawModal extends React.Component {
  state = {
    quantity: 0,
    selectedProduct: {
      id: '',
      name: '',
    },
  }

  constructor(props) {
    super(props)

    this.updateQuantity = this.updateQuantity.bind(this)
    this.withdrawProduct = this.withdrawProduct.bind(this)
    this.updateProductSelected = this.updateProductSelected.bind(this)
  }

  updateProductSelected({target: {value: productId}}) {
    this.setState({
      selectedProduct: this.props.products.find(product => product.id === productId),
    })
  }

  updateQuantity({target: {value: quantity}}) {
    this.setState({quantity})
  }

  withdrawProduct(e) {
    e.preventDefault()

    const {selectedProduct: {id: productId}, quantity} = this.state
    const {pantryId, onClose, onWithdraw} = this.props

    client
      .withdrawProductFromPantry(productId, pantryId, quantity)
      .then(onClose)
      .then(onWithdraw)
  }

  render() {
    const {open, products, onClose} = this.props
    const {quantity, selectedProduct} = this.state
    return (
      <ModalForm open={open} onClose={onClose} onSubmit={this.withdrawProduct}>
        <TextField select
                   required
                   label="Select a product"
                   value={selectedProduct.id}
                   onChange={this.updateProductSelected}>
          {products.map(buildProductItem)}
        </TextField>
        <TextField required
                   type="number"
                   label="Quantity"
                   value={quantity}
                   onChange={this.updateQuantity}/>
        <Button variant="contained" color="primary" type="submit">Withdraw</Button>
      </ModalForm>
    )
  }
}

PantryProductWithdrawModal.propTypes = {
  open: PropTypes.bool.isRequired,
  onClose: PropTypes.func.isRequired,
  pantryId: PropTypes.string.isRequired,
  onWithdraw: PropTypes.func.isRequired,
  products: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.string.isRequired,
      name: PropTypes.string.isRequired,
    })
  ).isRequired,
}

export default PantryProductWithdrawModal

function buildProductItem(product) {
  return (
    <MenuItem key={product.id} value={product.id}>
      {product.name}
    </MenuItem>
  )
}
