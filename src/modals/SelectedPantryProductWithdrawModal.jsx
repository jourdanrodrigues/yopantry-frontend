import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import {Button as MuiButton} from '@material-ui/core'

import client from '../api'
import {ModalForm as RawModalForm, TextField as CustomTextField} from '../components'

const TextField = styled(CustomTextField)``

const ModalForm = styled(RawModalForm)`
  ${TextField}:not(:first-child) {
    margin-top: .5em;
  }
`

const Button = styled(MuiButton)`
  margin-top: 2em !important;
`

class PantryProductWithdrawModal extends React.Component {
  state = {
    quantity: 0,
  }

  constructor(props) {
    super(props)

    this.updateQuantity = this.updateQuantity.bind(this)
    this.withdrawProduct = this.withdrawProduct.bind(this)
  }

  updateQuantity({target: {value: quantity}}) {
    this.setState({quantity})
  }

  withdrawProduct(e) {
    e.preventDefault()

    const {quantity} = this.state
    const {product: {id: productId}, pantryId, onClose, onWithdraw} = this.props

    client
      .withdrawProductFromPantry(productId, pantryId, quantity)
      .then(onClose)
      .then(onWithdraw)
  }

  render() {
    const {quantity} = this.state
    const {open, onClose, product} = this.props
    return (
      <ModalForm open={open} onClose={onClose} onSubmit={this.withdrawProduct}>
        <TextField required readOnly label="Product selected" value={product.name}/>
        <TextField required
                   type="number"
                   label="Quantity"
                   value={quantity}
                   onChange={this.updateQuantity}/>
        <Button variant="contained" color="primary" type="submit">Withdraw</Button>
      </ModalForm>
    )
  }
}

PantryProductWithdrawModal.defaultProps = {
  product: {
    id: '',
    name: '',
  },
}

PantryProductWithdrawModal.propTypes = {
  open: PropTypes.bool.isRequired,
  onClose: PropTypes.func.isRequired,
  pantryId: PropTypes.string.isRequired,
  onWithdraw: PropTypes.func.isRequired,
  product: PropTypes.shape({
    id: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
  }),
}

export default PantryProductWithdrawModal
