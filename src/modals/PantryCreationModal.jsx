import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import {Button as MuiButton} from '@material-ui/core'

import client from '../api'
import {ModalForm, TextField} from '../components'

const Button = styled(MuiButton)`
  margin-top: 2em !important;
`

class PantryCreationModal extends React.Component {
  state = {name: ''}

  constructor(props) {
    super(props)

    this.createPantry = this.createPantry.bind(this)
    this.updatePantryName = this.updatePantryName.bind(this)
  }

  updatePantryName({target: {value: name}}) {
    this.setState({name})
  }

  createPantry(e) {
    e.preventDefault()

    const data = {
      name: this.state.name,
    }

    client
      .createPantry(data)
      .then(this.props.onClose)
      .then(this.props.onCreation)
  }

  render() {
    const {open, onClose} = this.props
    return (
      <ModalForm open={open} onClose={onClose} onSubmit={this.createPantry}>
        <TextField required label="Pantry Name" value={this.state.name} onChange={this.updatePantryName}/>
        <Button variant="contained" color="primary" type="submit">Create</Button>
      </ModalForm>
    )
  }
}

PantryCreationModal.propTypes = {
  open: PropTypes.bool.isRequired,
  onClose: PropTypes.func.isRequired,
  onCreation: PropTypes.func.isRequired,
}

export default PantryCreationModal
