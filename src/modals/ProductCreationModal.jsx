import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import {Button as MuiButton} from '@material-ui/core'

import client from '../api'
import {
  BarCodeField as RawBarCodeField,
  FileField as RawFileField,
  ModalForm as RawModalForm,
  TextField as RawTextField
} from '../components'

const FileField = styled(RawFileField)`
  margin-top: 2em;
`

const TextField = styled(RawTextField)``
const BarCodeField = styled(RawBarCodeField)``

const ModalForm = styled(RawModalForm)`
  ${TextField}, ${BarCodeField} {
    &:not(:first-child) {
      margin-top: .5em;
    }
  }
`

const Button = styled(MuiButton)`
  margin-top: 2em !important;
`

class ProductCreationModal extends React.Component {
  state = {
    open: false,
    name: '',
    image: {
      name: '',
      file: null,
    },
    barCode: '',
  }

  constructor(props) {
    super(props)

    this.updateName = this.updateName.bind(this)
    this.updateImage = this.updateImage.bind(this)
    this.updateBarCode = this.updateBarCode.bind(this)
    this.createProduct = this.createProduct.bind(this)
  }

  updateName({target: {value: name}}) {
    this.setState({name})
  }

  updateBarCode({target: {value: barCode}}) {
    this.setState({barCode})
  }

  updateImage({target: element}) {
    this.setState({
      image: {
        name: element.value,
        file: element.files[0],
      }
    })
  }

  createProduct(e) {
    e.preventDefault()

    const {name, barCode, image} = this.state
    const data = {name, barCode, image: image.file || ''}

    client
      .createProduct(data)
      .then(this.props.onClose)
      .then(this.props.onCreation)
  }

  render() {
    const {open, onClose} = this.props
    const {name, barCode, image} = this.state
    return (
      <ModalForm open={open} onClose={onClose} onSubmit={this.createProduct}>
        <TextField required label="Name" value={name} onChange={this.updateName}/>
        <BarCodeField required label="Bar code" value={barCode} onChange={this.updateBarCode}/>
        <FileField fullWidth label="Upload image" color="secondary" value={image.name} onChange={this.updateImage}/>
        <Button variant="contained" color="primary" type="submit">Create</Button>
      </ModalForm>
    )
  }
}

ProductCreationModal.propTypes = {
  open: PropTypes.bool.isRequired,
  onClose: PropTypes.func.isRequired,
  onCreation: PropTypes.func,
}

export default ProductCreationModal
