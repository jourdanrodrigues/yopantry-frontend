import React from 'react'
import ReactDOM from 'react-dom'
import {Provider} from 'react-redux'
import {createMuiTheme, MuiThemeProvider} from '@material-ui/core/styles'

import './index.css'
import './sentrySetup'
import App from './App'
import store from './reduxStore'
import registerServiceWorker from './registerServiceWorker'

const theme = createMuiTheme({
  typography: {useNextVariants: true},
})

ReactDOM.render(
  <MuiThemeProvider theme={theme}>
    <Provider store={store}>
      <App/>
    </Provider>
  </MuiThemeProvider>,
  document.querySelector('.root'),
)
registerServiceWorker()
