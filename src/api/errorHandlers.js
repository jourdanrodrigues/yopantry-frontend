import store from '../reduxStore'
import {showSnackbar} from '../actions'
import {parseObjectAttrsToCamelCase, parseToReadable} from '../utils'

const betterErrorMessageMap = {
  'network error': 'There are some network issues. Can\'t contact the server.',
  'error decoding signature.': 'Your access needs to be refreshed.',
}

export {extractRequestError}

function extractRequestError(error) {
  let errorData

  if (error.response) {
    errorData = parseObjectAttrsToCamelCase(error.response.data)
    if (errorData.detail && !Array.isArray(errorData.detail)) {
      const errorMessage = getBetterErrorMessage(errorData.detail)
      errorData = {detail: [errorMessage]}
    }
  } else if (error.message) {
    const errorMessage = getBetterErrorMessage(error.message)
    errorData = {error: [errorMessage]}
  } else {
    errorData = error
  }

  displayErrorMessage(errorData)

  throw errorData
}

function displayErrorMessage(error) {
  const message = Object.entries(error).reduce(errorMessageReducer, []).join('\n')
  store.dispatch(showSnackbar(message))
}

function errorMessageReducer(messages, [key, value]) {
  const message = value.join('\n').replace(/his field/g, `he ${parseToReadable(key)}`)
  return messages.concat([message])
}

function getBetterErrorMessage(message) {
  return betterErrorMessageMap[message.toLowerCase()] || message
}
