import axios from 'axios'

import {isOnline} from './networkHelpers'
import {extractRequestError} from './errorHandlers'
import {cookies, parseObjectAttrsToCamelCase, parseObjectAttrsToSnakeCase, parseObjectToFormData} from '../utils'

const formDataContentType = 'multipart/form-data'

const client = axios.create({baseURL: process.env.REACT_APP_API_URL})
const interceptors = client.interceptors

export {
  interceptors,
  getResourceLogged,
  postResourceLogged,
  deleteResourceLogged,
  getResource,
  postResource,
  getFormDataContentTypeHeader,
  getAuthorizationHeader
}

function getAuthorizationHeader() {
  return {Authorization: `Token ${cookies.getToken()}`}
}

function getFormDataContentTypeHeader() {
  return {'Content-Type': formDataContentType}
}

function getResource(endpoint, headers = {}) {
  const resources = !isOnline() && JSON.parse(localStorage.getItem(endpoint))

  if (resources) {
    return new Promise(resolve => resolve(resources))
  }

  return client.get(endpoint, {headers})
    .catch(extractRequestError)
    .then(extractData)
    .then(data => {
      localStorage.setItem(endpoint, JSON.stringify(data))
      return data
    })

}

function postResource(endpoint, data, headers = {}) {
  data = prepareDataForUpload(data, headers)
  return client.post(endpoint, data, {headers}).catch(extractRequestError).then(extractData)
}

function deleteResource(endpoint, data = {}, headers = {}) {
  data = prepareDataForUpload(data, headers)
  return client.delete(endpoint, {data, headers}).catch(extractRequestError).then(extractData)
}

function getResourceLogged(endpoint) {
  return getResource(endpoint, getAuthorizationHeader())
}

function postResourceLogged(endpoint, data) {
  return postResource(endpoint, data, getAuthorizationHeader())
}

function deleteResourceLogged(endpoint, data) {
  return deleteResource(endpoint, data, getAuthorizationHeader())
}

function extractData(response) {
  return parseObjectAttrsToCamelCase(response.data)
}

function prepareDataForUpload(data, headers) {
  data = parseObjectAttrsToSnakeCase(data)

  if (headers['Content-Type'] === formDataContentType) {
    data = parseObjectToFormData(data)
  }

  return data
}
