let reloadSet = false

function isOnline() {
  if (navigator.onLine) {
    return true
  } else if (!reloadSet) {
    window.addEventListener('online', () => window.location.reload())
    reloadSet = true
  }
  return false
}

export {isOnline}
