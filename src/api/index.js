import {cookies} from '../utils'
import {
  deleteResourceLogged,
  getAuthorizationHeader,
  getFormDataContentTypeHeader,
  getResource,
  getResourceLogged,
  interceptors,
  postResource,
  postResourceLogged
} from './resourceHelpers'

export default {
  interceptors,
  // Auth
  signUp: _signUp,
  signIn: authenticate,
  refreshToken: _refreshToken,
  // Resources
  getPantries: () => getResourceLogged('/pantries/'),
  createPantry: data => postResourceLogged('/pantries/', data),
  getPantryProducts: pantryId => getResourceLogged(`/pantries/${pantryId}/products/`),
  getProductsByName: _getProductsByName,
  createProduct: _createProduct,
  addProductsToPantry: (pantryId, data) => postResourceLogged(`/pantries/${pantryId}/products/`, data),
  withdrawProductFromPantry: _withdrawProductFromPantry,
}

function _withdrawProductFromPantry(productId, pantryId, quantity) {
  return deleteResourceLogged(`/pantries/${pantryId}/products/${productId}/`, {quantity})
}

function _getProductsByName(name) {
  if (name.length < 3) {
    return new Promise(resolve => resolve([]))
  }
  return getResource('/products/?name=' + name)
}

function _createProduct(data) {
  const headers = {...getFormDataContentTypeHeader(), ...getAuthorizationHeader()}
  return postResource('/products/', data, headers)
}

function _signUp(data) {
  return postResource('/auth/register/', data).then(() => authenticate(data))
}

function authenticate(data) {
  return postResource('/auth/', data).then(setTokenFromResponse)
}

function _refreshToken() {
  const data = {token: cookies.getToken()}
  return postResource('/auth/refresh/', data).then(setTokenFromResponse)
}

function setTokenFromResponse(data) {
  cookies.setToken(data.token)
}
