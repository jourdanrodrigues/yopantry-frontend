import store from '../reduxStore'
import {showSnackbar} from '../actions'
import {extractRequestError} from './errorHandlers'

jest.mock('../reduxStore')

describe('API error extractor', () => {
  beforeEach(() => {
    store.dispatch.mockClear()
  })

  it('should dispatch "showSnackbar" action replacing "this field" and joining errors', () => {
    const errorObject = {
      quantity: ['Required is this field.'],
      firstName: ['This field is required.'],
    }

    callAndSuppressError(extractRequestError, errorObject)

    const message = 'Required is the quantity.\nThe first name is required.'
    const action = showSnackbar(message)
    expect(store.dispatch).toHaveBeenCalledWith(action)
  })

  it('should dispatch "showSnackbar" action from unmapped error', () => {
    const snackbarMessage = 'Some other error'
    const errorObject = {unknownKey: [snackbarMessage]}

    callAndSuppressError(extractRequestError, errorObject)

    const action = showSnackbar(snackbarMessage)
    expect(store.dispatch).toHaveBeenCalledWith(action)
  })

  it('should throw data from response', () => {
    const data = {randomKey: ['randomValue']}
    const errorObject = {
      response: {data},
    }

    const errorThrown = callEndGetErrorThrown(extractRequestError, errorObject)
    expect(errorThrown).toEqual(data)
  })

  it('should parse detail attribute from response data to array and throw data', () => {
    const errorObject = {
      response: {
        data: {detail: 'randomValue'},
      },
    }

    const errorThrown = callEndGetErrorThrown(extractRequestError, errorObject)
    expect(errorThrown).toEqual({detail: ['randomValue']})
  })

  it('should parse response data attributes to camel case and throw data', () => {
    const errorObject = {
      response: {
        data: {
          notParsed: ['1'],
          test_key_1: ['2'],
          another_attribute: ['3'],
        },
      },
    }

    const errorThrown = callEndGetErrorThrown(extractRequestError, errorObject)
    expect(errorThrown).toEqual({
      testKey1: ['2'],
      notParsed: ['1'],
      anotherAttribute: ['3'],
    })
  })

  it('should throw mapped message when not connected or server is not up', () => {
    const errorObject = {message: 'Network error'}

    const errorThrown = callEndGetErrorThrown(extractRequestError, errorObject)
    expect(errorThrown).toEqual({error: ['There are some network issues. Can\'t contact the server.']})
  })

  it('should throw mapped message when token fails to be decoded', () => {
    const errorObject = {message: 'Error decoding signature.'}

    const errorThrown = callEndGetErrorThrown(extractRequestError, errorObject)
    expect(errorThrown).toEqual({error: ['Your access needs to be refreshed.']})
  })
})

function callEndGetErrorThrown(func, ...args) {
  try {
    func(...args)
  } catch (exc) {
    checkError(exc)
    return exc
  }
}

function callAndSuppressError(func, ...args) {
  let exception
  try {
    func(...args)
  } catch (exc) {
    checkError(exc)
    exception = exc
  }

  expect(exception).not.toBeFalsy()
}

function checkError(exception) {
  if (!(exception instanceof Object)) {
    throw exception
  }
}
