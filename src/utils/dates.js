import {oneDayTime} from './vars'

export function daysDiff(newestDate, oldestDate) {
  const timeDiff = newestDate.getTime() - oldestDate.getTime()
  return Math.ceil(timeDiff / oneDayTime)
}
