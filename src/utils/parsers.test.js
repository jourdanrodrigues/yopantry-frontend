import {
  parseObjectAttrsToCamelCase,
  parseObjectAttrsToSnakeCase,
  parseObjectToFormData,
  parseToReadable
} from './parsers'

describe('Parser "parseObjectToFormData"', () => {
  it('should create a "FormData" from a plain object', () => {
    const object = {
      attr1: 'value1',
      attrNumber2: 'value2',
    }

    const formData = parseObjectToFormData(object)

    Object.entries(object).forEach(([key, value]) => {
      expect(formData.get(key)).toBe(value)
    })
  })
})

describe('Parser "parseObjectAttrsToCamelCase"', () => {
  it('should create a new object with camel case keys', () => {
    const object = {
      attr_1: 'value1',
      attr_number_2: 'value2',
      attr_number_3: 'value3',
    }
    const parsedObject = parseObjectAttrsToCamelCase(object)

    expect(parsedObject).toEqual({
      attr1: 'value1',
      attrNumber2: 'value2',
      attrNumber3: 'value3',
    })
  })

  it('should create a new object list with camel case keys', () => {
    const object = {
      attr_1: 'value1',
      attr_number_2: 'value2',
      attr_number_3: 'value3',
    }
    const objects = [object, object]

    const parsedObjects = parseObjectAttrsToCamelCase(objects)

    const expectedObject = {
        attr1: 'value1',
        attrNumber2: 'value2',
        attrNumber3: 'value3',
      }
    expect(parsedObjects).toEqual([expectedObject, expectedObject])
  })
})

describe('Parser "parseObjectAttrsToSnakeCase"', () => {
  it('should create a new object with snake case keys', () => {
    const object = {
      attr1: 'value1',
      attrNumber2: 'value2',
      attrNumber3: 'value3',
    }
    const parsedObject = parseObjectAttrsToSnakeCase(object)

    expect(parsedObject).toEqual({
      attr1: 'value1',
      attr_number2: 'value2',
      attr_number3: 'value3',
    })
  })
})

describe('Parser "parseToReadable"', () => {
  it('should return a string space separated string', () => {
    const parsedString = parseToReadable('some_snake_case_text')

    expect(parsedString).toBe('some snake case text')
  })
})
