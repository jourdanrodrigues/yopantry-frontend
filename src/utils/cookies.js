import {oneDayTime} from './vars'

const tokenKey = btoa(btoa('Token')).replace(/=/g, '')

// Source: https://www.w3schools.com/Js/js_cookies.asp
export const cookies = {
  getToken: () => getCookie(tokenKey),
  setToken: value => setCookie(tokenKey, value, 7),
  clearToken: () => setCookie(tokenKey, null, -1),
  getTokenExpirationDate: _getTokenExpirationDate,
}

function _getTokenExpirationDate() {
  const token = getCookie(tokenKey)
  const payload = JSON.parse(atob(token.split('.')[1]))
  return new Date(payload.exp * 1000)
}

function setCookie(key, value, expirationDays) {
  const expirationTime = new Date().getTime() + expirationDays * oneDayTime
  const expirationDate = new Date(expirationTime)
  document.cookie = `${key}=${value};expires=${expirationDate.toUTCString()};path=/`
}

function getCookie(key) {
  const _key = `${key}=`
  const cookies = document.cookie.split(';')
  const cookie = cookies.find(cookie => cookie.replace(' ', '').startsWith(_key))
  return cookie ? cookie.split('=')[1] : null
}
