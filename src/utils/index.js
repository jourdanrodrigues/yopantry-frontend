import {daysDiff} from './dates'
import {cookies} from './cookies'
import {forDesktop, forMobile} from './vars'
import {
  parseObjectAttrsToCamelCase,
  parseObjectAttrsToSnakeCase,
  parseObjectToFormData,
  parseToReadable
} from './parsers'

export {
  cookies,
  daysDiff,
  forDesktop,
  forMobile,
  parseObjectAttrsToCamelCase,
  parseObjectAttrsToSnakeCase,
  parseObjectToFormData,
  parseToReadable,
}

export function delayCall(func, delay) {
  let delayedCallTimeoutId = null

  return function _callDelayed(...params) {
    if (delayedCallTimeoutId) {
      clearTimeout(delayedCallTimeoutId)
    }
    delayedCallTimeoutId = setTimeout(() => {
      func(...params)
      delayedCallTimeoutId = null
    }, delay)
  }
}

