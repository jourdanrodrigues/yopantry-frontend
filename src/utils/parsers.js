export {parseObjectToFormData, parseObjectAttrsToCamelCase, parseObjectAttrsToSnakeCase, parseToReadable}

function parseObjectToFormData(object) {
  return Object.entries(object).reduce(objectToFormDataReducer, new FormData())
}

function parseObjectAttrsToCamelCase(object) {
  return parseObjectAttrsToCase(object, parseToCamelCase)
}

function parseObjectAttrsToSnakeCase(object) {
  return parseObjectAttrsToCase(object, parseToSnakeCase)
}

function parseToReadable(string) {
  return string.replace(/_(.)/g, (_, char) => ` ${char}`).replace(/[A-Z]/g, char => ` ${char.toLowerCase()}`)
}

function parseToCamelCase(string) {
  return string.replace(/_(.)/g, (_, char) => char.toUpperCase())
}

function parseToSnakeCase(string) {
  return string.replace(/[A-Z]/g, char => `_${char.toLowerCase()}`)
}

function parseObjectAttrsToCase(object, caseParser) {
  if (object instanceof Array) {
    return object.map(item => parseObjectAttrsToCase(item, caseParser))
  }
  return Object.entries(object).reduce(getCaseReducer(caseParser), {})
}

function getCaseReducer(caseParser) {
  return function _reducer(newObject, [key, value]) {
    const newKey = caseParser(key)
    return Object.assign({}, newObject, {[newKey]: value})
  }
}

function objectToFormDataReducer(formData, [key, value]) {
  formData.append(key, value)
  return formData
}
