export const oneDayTime = 24 * 60 * 60 * 1000
export const forMobile = '@media (max-width: 767px)'
export const forDesktop = '@media (min-width: 768px)'
