import React from 'react'
import PropTypes from 'prop-types'
import {connect} from 'react-redux'
import styled from 'styled-components'
import {AccountCircle, Menu as MenuIcon, KeyboardBackspace as KeyboardBackspaceIcon} from '@material-ui/icons'
import {AppBar as MuiAppBar, IconButton, Menu, MenuItem, Toolbar as MuiToolbar, Typography} from '@material-ui/core'

import Drawer from './Drawer'
import client from '../api'
import {cookies, daysDiff} from '../utils'

const Toolbar = styled(MuiToolbar)`
  display: flex;
  justify-content: space-between;
`

const Title = styled(Typography)`
  flex-grow: 1;
`

const MenuButton = styled(IconButton)`
    margin-left: -12px !important;
    margin-right: 20px !important;
`

// If this is needed somewhere else, use Redux
let tokenExpirationDate
let refreshingAuth = false

class AppBar extends React.Component {
  state = {
    drawerOpen: false,
    accountMenuAnchor: null,
  }

  constructor(props) {
    super(props)

    this.logOut = this.logOut.bind(this)
    this.openDrawer = this.openDrawer.bind(this)
    this.closeDrawer = this.closeDrawer.bind(this)
    this.openAccountMenu = this.openAccountMenu.bind(this)
    this.closeAccountMenu = this.closeAccountMenu.bind(this)
  }

  componentWillMount() {
    if (!cookies.getToken()) {
      this.props.history.push('/login/')
      return
    }
    const {request, response} = client.interceptors
    request.use(_requestInterceptor.bind(this))
    response.use(response => response, _responseErrorInterceptor.bind(this))
  }

  openAccountMenu = e => {
    this.setState({accountMenuAnchor: e.currentTarget})
  }

  closeAccountMenu = () => {
    this.setState({accountMenuAnchor: null})
  }

  openDrawer = () => {
    this.setState({drawerOpen: true})
  }

  closeDrawer = () => {
    this.setState({drawerOpen: false})
  }

  logOut() {
    cookies.clearToken()
    this.props.history.push('/login/')
  }

  redirectTo(path) {
    return () => this.props.history.push(path)
  }

  render() {
    const {accountMenuAnchor} = this.state
    const accountMenuOpen = !!accountMenuAnchor

    const {title, backPage} = this.props

    let menuIcon, onMenuButtonClick

    if (backPage) {
      menuIcon = <KeyboardBackspaceIcon/>
      onMenuButtonClick = this.redirectTo(backPage)
    } else {
      menuIcon = <MenuIcon/>
      onMenuButtonClick = this.openDrawer
    }

    return (
      <MuiAppBar position="static">
        <Drawer open={this.state.drawerOpen} onClose={this.closeDrawer} history={this.props.history}/>
        <Toolbar>
          <MenuButton onClick={onMenuButtonClick} color="inherit" aria-label="Menu">
            {menuIcon}
          </MenuButton>
          <Title variant="h6" color="inherit">{title}</Title>
          <div>
            <IconButton color="inherit" onClick={this.openAccountMenu} aria-haspopup="true">
              <AccountCircle/>
            </IconButton>
            <Menu open={accountMenuOpen} anchorEl={accountMenuAnchor} onClose={this.closeAccountMenu}>
              <MenuItem onClick={this.logOut}>Log Out</MenuItem>
            </Menu>
          </div>
        </Toolbar>
      </MuiAppBar>
    )
  }
}

AppBar.propTypes = {
  title: PropTypes.string.isRequired,
  backPage: PropTypes.string,
}

export default connect(mapStateToProps)(AppBar)

function mapStateToProps(state) {
  return {
    title: state.title,
    backPage: state.backPage
  }
}

function _requestInterceptor(config) {
  const token = cookies.getToken()
  if (!token) {
    return config
  }

  if (!tokenExpirationDate) {
    tokenExpirationDate = cookies.getTokenExpirationDate()
  }

  const expiresToday = daysDiff(tokenExpirationDate, new Date()) === 1
  if (!refreshingAuth && expiresToday) {
    // This does not block the current request, since it's still valid
    refreshingAuth = true
    client.refreshToken().finally(() => {
      refreshingAuth = false
    })
  }

  return config
}

function _responseErrorInterceptor(error) {
  if (error.response && error.response.status === 401) {
    cookies.clearToken()
    this.props.history.push('/login/')
  }
  throw error
}
