import React from 'react'
import PropTypes from 'prop-types'
import {Home, ShoppingBasket, Store} from '@material-ui/icons'
import {Divider, Drawer as MuiDrawer, List, ListItem, ListItemIcon, ListItemText} from '@material-ui/core'

class Drawer extends React.Component {
  redirectTo(path) {
    const history = this.props.history
    return () => {
      if (history.location.pathname !== path) {
        history.push(path)
      }
    }
  }

  getItems() {
    return [
      {text: 'Pantries', icon: <Home/>, redirect: this.redirectTo('/app/pantries/')},
      {divider: 1},
      {text: 'Products', icon: <ShoppingBasket/>, redirect: this.redirectTo('/app/products/')},
      {text: 'Stores', icon: <Store/>, redirect: this.redirectTo('/app/pantries/')},
    ].map(buildItem)
  }

  render() {
    const {open, onClose} = this.props
    return (
      <MuiDrawer open={open} onClose={onClose}>
        <div tabIndex={0} role="button" onClick={onClose} onKeyDown={onClose}>
          <List>
            {this.getItems()}
          </List>
        </div>
      </MuiDrawer>
    )
  }
}

Drawer.propTypes = {
  open: PropTypes.bool.isRequired,
  onClose: PropTypes.func.isRequired,
  history: PropTypes.object.isRequired,
}

export default Drawer

function buildItem(item) {
  if (item.divider) {
    return <Divider key={item.divider}/>
  }
  return (
    <ListItem button key={item.text} onClick={item.redirect}>
      <ListItemIcon>{item.icon}</ListItemIcon>
      <ListItemText primary={item.text}/>
    </ListItem>
  )
}
