import React from 'react'
import styled from 'styled-components'
import {MenuItem} from '@material-ui/core'
import {Add as AddIcon} from '@material-ui/icons'

import View from './View'
import client from '../api'
import {delayCall, forMobile} from '../utils'
import {ProductCreationModal as CreationModal} from '../modals'
import {ContextMenu, FloatingActionButton, ProductCard, ProductList, TextField as RawTextField} from '../components'

const TextFieldWrapper = styled.div`
  display: flex;
  justify-content: center;
  margin: 1em 1em 0 1em !important;
`

const TextField = styled(RawTextField)`
  width: 40%;
  
  ${forMobile} {
    width: 100%;
  }
`

class ProductView extends React.Component {
  state = {
    name: '',
    products: [],
    creationModalOpen: false,
    contextMenuAnchorEl: null,
  }

  constructor(props) {
    super(props)

    this.updateName = this.updateName.bind(this)
    this.openContextMenu = this.openContextMenu.bind(this)
    this.buildProductItem = this.buildProductItem.bind(this)
    this.closeContextMenu = this.closeContextMenu.bind(this)
    this.openCreationModal = this.openCreationModal.bind(this)
    this.closeCreationModal = this.closeCreationModal.bind(this)
    this.fetchProductsByName = delayCall(this.fetchProductsByName.bind(this), 500)
  }

  fetchProductsByName(name) {
    client
      .getProductsByName(name)
      .then(products => {
        this.setState({products})
      })
  }

  openCreationModal() {
    this.setState({creationModalOpen: true})
  }

  closeCreationModal() {
    this.setState({creationModalOpen: false})
  }

  openContextMenu(e) {
    this.setState({contextMenuAnchorEl: e.target})
  }

  closeContextMenu() {
    this.setState({contextMenuAnchorEl: null})
  }

  updateName({target: {value: name}}) {
    this.setState({name})
    this.fetchProductsByName(name)
  }

  buildProductItem(product) {
    return <ProductCard key={product.id} product={product} onLongClick={this.openContextMenu} label={product.name}/>
  }

  render() {
    const {name, products, creationModalOpen, contextMenuAnchorEl} = this.state
    return (
      <View title="Products">
        <TextFieldWrapper>
          <TextField label="Name" value={name} onChange={this.updateName}/>
        </TextFieldWrapper>
        <ProductList>
          {products.map(this.buildProductItem)}
        </ProductList>
        <FloatingActionButton aria-label="add product" onClick={this.openCreationModal}>
          <AddIcon/>
        </FloatingActionButton>
        <CreationModal open={creationModalOpen} onClose={this.closeCreationModal}/>
        <ContextMenu anchorEl={contextMenuAnchorEl} onClose={this.closeContextMenu}>
          <MenuItem>Edit</MenuItem>
        </ContextMenu>
      </View>
    )
  }
}

export default ProductView
