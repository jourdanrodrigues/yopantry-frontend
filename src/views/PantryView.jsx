import React from 'react'
import styled from 'styled-components'
import {Add as AddIcon} from '@material-ui/icons'
import {List, ListItem, ListItemText as MuiListItemText} from '@material-ui/core'

import View from './View'
import client from '../api'
import {forDesktop} from '../utils'
import {FloatingActionButton} from '../components'
import {PantryCreationModal as CreationModal} from '../modals'

const ListItemText = styled(MuiListItemText)`
  ${forDesktop} {
    text-align: center;
  }
`

class PantryView extends React.Component {
  state = {
    pantries: [],
    creationModalOpen: false,
  }

  constructor(props) {
    super(props)

    this.buildPantry = this.buildPantry.bind(this)
    this.fetchPantries = this.fetchPantries.bind(this)
    this.openCreationModal = this.openCreationModal.bind(this)
    this.closeCreationModal = this.closeCreationModal.bind(this)

    this.fetchPantries()
  }

  fetchPantries() {
    client
      .getPantries()
      .then(pantries => {
        this.setState({pantries})
      })
  }

  openCreationModal() {
    this.setState({creationModalOpen: true})
  }

  buildPantry(pantry) {
    const goToProducts = () => this.props.history.push(`/app/pantries/${pantry.id}/products/`)
    return (
      <ListItem key={pantry.id} button onClick={goToProducts}>
        <ListItemText primary={pantry.name}/>
      </ListItem>
    )
  }

  closeCreationModal() {
    this.setState({creationModalOpen: false})
  }

  render() {
    return (
      <View title="Pantries">
        <List>
          {this.state.pantries.map(this.buildPantry)}
        </List>
        <FloatingActionButton aria-label="add pantry" onClick={this.openCreationModal}>
          <AddIcon/>
        </FloatingActionButton>
        <CreationModal onCreation={this.fetchPantries}
                       onClose={this.closeCreationModal}
                       open={this.state.creationModalOpen}/>
      </View>
    )
  }
}

export default PantryView
