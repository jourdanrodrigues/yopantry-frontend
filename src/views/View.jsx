import React from 'react'
import PropTypes from 'prop-types'
import {connect} from 'react-redux'
import {bindActionCreators} from 'redux'

import {setTitle} from '../actions'

class View extends React.Component {
  constructor(props) {
    super(props)

    const {title, setTitle} = props
    setTitle(title)
  }

  render() {
    const {children, className} = this.props
    return <div className={className}>{children}</div>
  }
}

View.propTypes = {
  className: PropTypes.string,
  title: PropTypes.string.isRequired,
  setTitle: PropTypes.func.isRequired,
  children: PropTypes.oneOfType([
    PropTypes.node,
    PropTypes.arrayOf(PropTypes.node),
  ]).isRequired
}

export default connect(null, mapDispatchToProps)(View)

function mapDispatchToProps(dispatch) {
  return bindActionCreators({setTitle}, dispatch)
}

