import React from 'react'
import PropTypes from 'prop-types'
import {connect} from 'react-redux'
import styled from 'styled-components'
import {bindActionCreators} from 'redux'
import {Button as MuiButton, FormControlLabel as MuiFormControlLabel, Switch} from '@material-ui/core'

import client from '../api'
import {TextField} from '../components'
import {cookies, forMobile} from '../utils'
import {showSnackbar} from '../actions/snackbar'

const Centralizer = styled.div`
  top: 20%;
  width: 100%;
  display: flex;
  position: absolute;
  align-items: center;
  justify-content: center;
  
  ${forMobile} {
    top: 10%;
  }
`

const Wrapper = styled.form`
  width: 20em;
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  
  ${forMobile} {
    width: 90%;
  }
`

const Button = styled(MuiButton)`
  margin-top: 2em !important;
  min-height: 40px !important;
  
  ${forMobile} {
    min-height: 50px !important;
  }
`

const Fields = styled.div`
  width: 100%;
  display: flex;
  flex-direction: column;

  *:not(:first-child) {
    margin-top: 1em;
  }
`

const FormControlLabel = styled(MuiFormControlLabel)`
  margin-top: .5em;
  margin-right: 0 !important;
`

class AuthPage extends React.Component {
  constructor(props) {
    super(props)

    this.updateEmail = this.updateEmail.bind(this)
    this.authenticate = this.authenticate.bind(this)
    this.goToPantries = this.goToPantries.bind(this)
    this.updateUsername = this.updateUsername.bind(this)
    this.updatePassword = this.updatePassword.bind(this)
    this.toggleRegister = this.toggleRegister.bind(this)
  }

  state = {
    email: '',
    username: '',
    password: '',
    isRegister: false,
  }

  componentWillMount() {
    if (cookies.getToken()) {
      this.goToPantries()
    }
  }

  updateEmail({target: {value: email}}) {
    this.setState({email})
  }

  updateUsername({target: {value: username}}) {
    this.setState({username})
  }

  updatePassword({target: {value: password}}) {
    this.setState({password})
  }

  goToPantries() {
    this.props.history.push('/app/pantries/')
  }

  toggleRegister({target}) {
    this.setState({isRegister: target.checked})
  }

  authenticate(e) {
    e.preventDefault()

    const {username, email, password} = this.state
    const data = {username, email, password}

    const authenticate = this.state.isRegister ? client.signUp : client.signIn

    authenticate(data).then(this.goToPantries)
  }

  getEmailField() {
    return this.state.isRegister
      ? (
        <TextField required
                   label="Email"
                   spellCheck="false"
                   onClick={selectInputContent}
                   value={this.state.email}
                   onChange={this.updateEmail}/>
      )
      : null
  }

  render() {
    return (
      <Centralizer>
        <Wrapper onSubmit={this.authenticate}>
          <Fields>
            <TextField required
                       label="Username"
                       spellCheck="false"
                       onClick={selectInputContent}
                       value={this.state.username}
                       onChange={this.updateUsername}/>
            {this.getEmailField()}
            <TextField required
                       type="password"
                       label="Password"
                       onClick={selectInputContent}
                       value={this.state.password}
                       onChange={this.updatePassword}/>
          </Fields>
          <Button variant="contained" color="primary" type="submit">
            Sign {this.state.isRegister ? 'Up' : 'In'}
          </Button>
          <FormControlLabel control={<Switch onClick={this.toggleRegister}/>} label="Creating an account"/>
        </Wrapper>
      </Centralizer>
    )
  }
}

AuthPage.propTypes = {
  showSnackbar: PropTypes.func.isRequired,
}

export default connect(null, mapDispatchToProps)(AuthPage)

function mapDispatchToProps(dispatch) {
  return bindActionCreators({showSnackbar}, dispatch)
}

function selectInputContent({target}) {
  target.select()
}
