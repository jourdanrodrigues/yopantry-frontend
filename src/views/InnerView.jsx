import React from 'react'
import PropTypes from 'prop-types'
import {connect} from 'react-redux'
import {bindActionCreators} from 'redux'

import View from './View'
import {setBackPage, unsetBackPage} from '../actions'

class InnerView extends React.Component {
  constructor(props) {
    super(props)

    const {setBackPage, backPage} = props
    setBackPage(backPage)
  }

  componentWillUnmount() {
    this.props.unsetBackPage()
  }

  render() {
    const {children, title, className} = this.props
    return <View className={className} title={title}>{children}</View>
  }
}

InnerView.propTypes = {
  className: PropTypes.string,
  title: PropTypes.string.isRequired,
  backPage: PropTypes.string.isRequired,
  setBackPage: PropTypes.func.isRequired,
  unsetBackPage: PropTypes.func.isRequired,
  children: PropTypes.oneOfType([
    PropTypes.node,
    PropTypes.arrayOf(PropTypes.node),
  ]).isRequired
}

export default connect(null, mapDispatchToProps)(InnerView)

function mapDispatchToProps(dispatch) {
  return bindActionCreators({setBackPage, unsetBackPage}, dispatch)
}
