import React from 'react'
import PropTypes from 'prop-types'
import {connect} from 'react-redux'
import styled from 'styled-components'
import {bindActionCreators} from 'redux'
import {MenuItem} from '@material-ui/core'
import {Add as AddIcon, Remove as RemoveIcon} from '@material-ui/icons'

import client from '../api'
import InnerView from './InnerView'
import {setBackPage, setTitle, unsetBackPage} from '../actions'
import {ContextMenu, FloatingActionButton as FAB, ProductCard, ProductList} from '../components'
import {
  PantryProductAdditionModal as AdditionModal,
  PantryProductWithdrawModal as WithdrawModal,
  SelectedPantryProductWithdrawModal as SelectedWithdrawModal,
} from '../modals'

const TopFAB = styled(FAB)`
  bottom: 6em;
`

class PantryProductView extends React.Component {
  state = {
    products: [],
    additionModalOpen: false,
    withdrawModalOpen: false,
    selectedWithdrawModalOpen: false,
    contextMenuAnchorEl: null,
    contextMenuProduct: {
      id: '',
      name: '',
    },
  }

  constructor(props) {
    super(props)

    this.openContextMenu = this.openContextMenu.bind(this)
    this.buildProductItem = this.buildProductItem.bind(this)
    this.closeContextMenu = this.closeContextMenu.bind(this)
    this.openAdditionModal = this.openAdditionModal.bind(this)
    this.openWithdrawModal = this.openWithdrawModal.bind(this)
    this.closeWithdrawModal = this.closeWithdrawModal.bind(this)
    this.closeAdditionModal = this.closeAdditionModal.bind(this)
    this.fetchPantryProducts = this.fetchPantryProducts.bind(this)
    this.openSelectedWithdrawModal = this.openSelectedWithdrawModal.bind(this)
    this.closeSelectedWithdrawModal = this.closeSelectedWithdrawModal.bind(this)

    this.fetchPantryProducts()
  }

  fetchPantryProducts() {
    const {pantryId} = this.props.match.params
    client
      .getPantryProducts(pantryId)
      .then(products => {
        this.setState({products})
      })
  }

  openAdditionModal() {
    this.setState({additionModalOpen: true})
  }

  closeAdditionModal() {
    this.setState({additionModalOpen: false})
  }

  openWithdrawModal() {
    this.setState({withdrawModalOpen: true})
  }

  closeWithdrawModal() {
    this.setState({withdrawModalOpen: false})
  }

  openSelectedWithdrawModal() {
    this.setState({
      selectedWithdrawModalOpen: true,
      contextMenuAnchorEl: null,
    })
  }

  closeSelectedWithdrawModal() {
    this.setState({selectedWithdrawModalOpen: false})
  }

  openContextMenu(event, product) {
    this.setState({
      contextMenuProduct: product,
      contextMenuAnchorEl: event.target,
    })
  }

  closeContextMenu() {
    this.setState({
      contextMenuAnchorEl: null,
    })
  }

  buildProductItem(product) {
    const label = `${product.name} (${product.quantity})`
    return <ProductCard key={product.id} product={product} onLongClick={this.openContextMenu} label={label}/>
  }

  getWithdrawButton() {
    const thereIsProductsToWithdraw = this.state.products.find(product => product.quantity)
    if (!thereIsProductsToWithdraw) {
      return null
    }

    return (
      <TopFAB color="secondary" aria-label="remove product from pantry" onClick={this.openWithdrawModal}>
        <RemoveIcon/>
      </TopFAB>
    )
  }

  render() {
    const {
      products,
      additionModalOpen,
      withdrawModalOpen,
      contextMenuProduct,
      contextMenuAnchorEl,
      selectedWithdrawModalOpen,
    } = this.state
    const withdrawButton = this.getWithdrawButton()
    const {pantryId} = this.props.match.params
    return (
      <InnerView title="Pantry Products" backPage="/app/pantries/">
        <ProductList>
          {products.map(this.buildProductItem)}
        </ProductList>
        <FAB aria-label="add products to pantry" onClick={this.openAdditionModal}>
          <AddIcon/>
        </FAB>
        {withdrawButton}
        <AdditionModal pantryId={pantryId}
                       open={additionModalOpen}
                       onClose={this.closeAdditionModal}
                       onAddition={this.fetchPantryProducts}/>
        <WithdrawModal products={products}
                       pantryId={pantryId}
                       open={withdrawModalOpen}
                       onClose={this.closeWithdrawModal}
                       onWithdraw={this.fetchPantryProducts}/>
        <SelectedWithdrawModal product={contextMenuProduct}
                               pantryId={pantryId}
                               open={selectedWithdrawModalOpen}
                               onWithdraw={this.fetchPantryProducts}
                               onClose={this.closeSelectedWithdrawModal}/>
        <ContextMenu anchorEl={contextMenuAnchorEl} onClose={this.closeContextMenu}>
          <MenuItem onClick={this.openSelectedWithdrawModal}>Withdraw</MenuItem>
        </ContextMenu>
      </InnerView>
    )
  }
}

PantryProductView.propTypes = {
  setTitle: PropTypes.func.isRequired,
  setBackPage: PropTypes.func.isRequired,
  unsetBackPage: PropTypes.func.isRequired,
}

export default connect(null, mapDispatchToProps)(PantryProductView)

function mapDispatchToProps(dispatch) {
  return bindActionCreators({setBackPage, setTitle, unsetBackPage}, dispatch)
}
