const fs = require('fs')
const path = require('path')
const express = require('express')

if (!fs.existsSync(path.join(__dirname, 'build'))) {
  throw new Error('You must run "npm run build" first.')
}

const PRODUCTION = process.env.NODE_ENV === 'production'
const PORT = process.env.PORT || 3000

const app = express()

if (PRODUCTION) {
  app.use(httpsMiddleware)
}

app.use(express.static('build'))

app.listen(PORT, () => console.log(`Listening on port ${PORT}!`))

function httpsMiddleware(req, res, next) {
  if (req.headers['x-forwarded-proto'] === 'https') {
    return next()
  } else {
    res.redirect('https://' + req.headers.host + req.url)
  }
}
